# README #

AUGLO (Augmented Globe)

* Project's public Web Page: http://auglobal-stats.tk/AUGLO_project/
* Project Blog page: https://auglo.wordpress.com/
* User Manual: http://auglobal-stats.tk/AUGLO_project/Documents/User%20Manual/Auglo-User%20Manual.html
* Scholarly HTML Report: http://auglobal-stats.tk/AUGLO_project/Documents/Technical%20Report/Auglo-Technical%20Report.html

Technologies used:

* aframe
* aframe-graph-component
* webgl-globe
* aframe-gamepad-controls