/* global AFRAME */

AFRAME.registerComponent('change-bar', {
  schema: {
    on: {type: 'string'},
    target: {type: 'selector'},
    src: {type: 'string'},
    dur: {type: 'number', default: 300}
  },

  init: function () {
    var data = this.data;
    var el = this.el;

    el.addEventListener(data.on, function () {
      setTimeout(function () {
        console.log("OK - CHANGE_BAR");
        console.log(data.src);

        var alpha = 0.6;
    // fake data;
    var dd;
    if (data.src == 1) {
      dd = [ 181, 910, 320, 263, 162, 498, 331, 472, 414];
    }
    if (data.src == 2) {
      dd = [ 9.55, 6.35, 24.95, 8.50, 8.45, 15.40, 10.90, 10.55, 8.80];
    }
    if (data.src == 3) {
      dd = [ 77.8, 77.7, 62.9, 77.4, 77.7, 78.4, 78.4, 70.9, 69.9];
    }
     var animals = [ 'Belgium', 'Austria', 'Brazil', 'Germany', 'UK', 'Greece',
     'Croatia', 'Bulgaria', 'Romania']; 
    // we scale the height of our bars using d3's linear scale
    var hscale = d3.scale.linear()
        .domain([0, d3.max(dd)])
        .range([0, 3])
    
    // we select the scene object just like an svg
    var scene = d3.select("a-scene")
    
    // we use d3's enter/update/exit pattern to draw and bind our dom elements
    var bars = scene.selectAll("a-box.bar").data(dd)
    $( ".bar" ).append( "<a-text> </a-text>" );
    console.log("BARS:");
    console.log(bars);

    bars.attr({
      position: function(d,i) {
        var x = i*.75 + 8
        var y = hscale(d)/2;
        var z = 5
        return x + " " + y + " " + z
      },
      width: function(d) { return 0.5},
      depth: function(d) { return 0.5},
      height: function(d) { return hscale(d)},
      opacity: alpha,
      color: 'cyan'
    })
    .on("mouseenter", function(d,i) {
      // this event gets fired continuously as long as the cursor
      // is over the element. we only want trigger our animation the first time
      if(this.hovering) return;
      console.log("hover", i,d)
      this.hovering = true;
      d3.select(this).transition().duration(10)
      .attr({
        metalness: 0.8,
        opacity: .9
      })
       d3.select(this).select("a-text")
      .attr({
                'color':'hsla(180, 100%, 50%, 0.6)',
                'align':'center',
                'position':'0 '+ (hscale(d)/2+.5) + ' 0',
                'scale':'1 1 1',
                'value': animals[i]+', '+d
      })
    })
    .on("mouseleave", function(d,i) {
      console.log("leave",i,d)
      this.hovering = false;
      d3.select(this).transition().duration(500)
      .attr({
        metalness: 0,
        opacity: alpha
      })
       d3.select(this).select("a-text")
      .attr({
                'color':'cyan',
                'align':'center',
                'position':'0 '+ (hscale(d)/2+.5) + ' 0',
                'scale':'.01 .01 .01',
                'value': d
      })
    })

      }, data.dur);
    });
  }
});
/**
 * Component that listens to an event, fades out an entity, swaps the texture, and fades it
 * back in.
 */
AFRAME.registerComponent('set-image', {
  schema: {
    on: {type: 'string'},
    target: {type: 'selector'},
    src: {type: 'string'},
    dur: {type: 'number', default: 300}
  },

  init: function () {
    var data = this.data;
    var el = this.el;

    this.setupFadeAnimation();

    el.addEventListener(data.on, function () {
      // Fade out image.
      data.target.emit('set-image-fade');
      // Wait for fade to complete.
      setTimeout(function () {
        // Set image.
        data.target.setAttribute('material', 'src', data.src);
        console.log("OK");

      }, data.dur);
    });
  },

  /**
   * Setup fade-in + fade-out.
   */
  setupFadeAnimation: function () {
    var data = this.data;
    var targetEl = this.data.target;

    // Only set up once.
    if (targetEl.dataset.setImageFadeSetup) { return; }
    targetEl.dataset.setImageFadeSetup = true;

    // Create animation.
    targetEl.setAttribute('animation__fade', {
      property: 'material.color',
      startEvents: 'set-image-fade',
      dir: 'alternate',
      dur: data.dur,
      from: '#FFF',
      to: '#000'
    });
  }
});