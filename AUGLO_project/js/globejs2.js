   var last_commands = [];

    var cuv_idx;
    var voice = false;
    cuv_idx=0;
    var recognition = new webkitSpeechRecognition();
    recognition.continuous = true;
    //recognition.interimResults = true;
    recognition.onresult = function(event) { 
      console.log(event['results'][cuv_idx]['0']['transcript']); 
      //console.log(event);
      last_commands.push(event['results'][cuv_idx]['0']['transcript'].toLowerCase().trim());
      if (last_commands.length > 5) {
        last_commands.shift();
      }
      var last_commands_p = document.getElementById("last_commands");
      var txt = "";
      if (last_commands.length > 0) {
        txt = last_commands[0];
      }
      for (i=1;i<last_commands.length; ++i) {
        txt = txt + "<br>" + last_commands[i];
      }
      last_commands_p.innerHTML = txt;
      console.log(last_commands);
      if (event['results'][cuv_idx]['0']['transcript'].toLowerCase().trim() == 'zoom in') {
        globe.zoom(200);
      }
      if (event['results'][cuv_idx]['0']['transcript'].toLowerCase().trim() == 'zoom out') {
        globe.zoom(-200);
      }
      var command = event['results'][cuv_idx]['0']['transcript'].toLowerCase().trim();
      if (command.indexOf('focus') != -1) {
          var request = new XMLHttpRequest();
          var city = command.substring(command.indexOf("focus"));
          console.log("Address: " + city);

              var method = 'GET';
              var url = 'http://maps.googleapis.com/maps/api/geocode/json?address='+ city + '&sensor=true';
              var async = true;

              request.open(method, url, async);
              request.onreadystatechange = function(){
                if(request.readyState == 4 && request.status == 200){
                  var data = JSON.parse(request.responseText);
                  var address = data.results[0];
                  console.log(address);
                  var lon = address.geometry.location.lng;
                  var lat = address.geometry.location.lat;
                  globe.center(lat, lon);
                  document.getElementById("city_name").innerHTML = city;
                }
              };
              request.send();
      }
      console.log(event);
      cuv_idx++;
    }
    function resetVoiceRecog() {
      recognition.stop();
    }
    setInterval(resetVoiceRecog, 10000);
    recognition.onend = function(event) {
      console.log(voice);
      if (voice) {
        cuv_idx = 0;
        console.log("RECOGNITION STARTED");
        recognition.start();
      }
    }
    document.getElementById("mic_enabled").addEventListener("click", function(){
        if (voice == false) {
          voice = true;
          recognition.start();
          console.log("Mic enabled");
        } else {
          cuv_idx = 0;
          voice = false;
          recognition.stop();
          console.log("Mic disabled");
        }
    });